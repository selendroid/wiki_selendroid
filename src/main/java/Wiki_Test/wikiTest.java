package Wiki_Test;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.selendroid.client.SelendroidDriver;
import io.selendroid.common.SelendroidCapabilities;
import io.selendroid.common.device.DeviceTargetPlatform;
import io.selendroid.standalone.SelendroidConfiguration;
import io.selendroid.standalone.SelendroidLauncher;
import Wiki_Test.wikiTest;



public class wikiTest {
    RemoteWebDriver driver;
    String path;
    public static String result;
    public static String device="Android";
    public static String deviceName="OnePlus 6";
    public static String platformVersion="9";
    public static String platformName="Android";

    public static ExtentReports report;

    public static ExtentTest logger;


	/**
	 * Setup the environment before testing
	 * 
	 * @throws Exception
	 */
	
	public void setUp() throws Exception {

		
		
		

	}
	
	
	public void setup() throws Exception
	{
	    
	System.out.println("Session is creating");
	DesiredCapabilities capabilites = new DesiredCapabilities();
	capabilites.setCapability("deviceName", deviceName);
	capabilites.setCapability("platformVersion", platformVersion);
	capabilites.setCapability("platformName", platformName);
	capabilites.setCapability("appPackage", "org.wikipedia");
	capabilites.setCapability("appActivity", "org.wikipedia.main.MainActivity");


	// Start selendroid-standalone during test
			SelendroidConfiguration config = new SelendroidConfiguration();

			// Add the selendroid-test-app to the standalone server
			config.addSupportedApp("src/Wikipedia.apk");

			// start the standalone server
			SelendroidLauncher selendroidServer = new SelendroidLauncher(config);
			selendroidServer.launchSelendroid();

			// Create the selendroid capabilities
			SelendroidCapabilities capa = new SelendroidCapabilities("APP_ID");

			// Specify to use selendroid's test app
			capa.setAut("org.wikipedia:2.7.246-r2018-11-05");

			

			// Don't request simulator, use real device
			capa.setEmulator(false);

			// capa.wait(10000000);

			// Create instance of Selendroid Driver
			driver = new SelendroidDriver(capa);
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	}

	public void starter() throws Exception{
		String timeStamp = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(Calendar.getInstance().getTime());
		Path path = Paths.get("user.dir/Result/Test_"+timeStamp+".html");
		Files.createDirectories(path.getParent());
		Files.createFile(path);
		String pa = path.toString();
		report = new ExtentReports(pa);
		
	}

	public void search() throws Exception 
	{
		
		
	   

		
		
		logger = report.startTest("Sreach Topic");
		
		try {
		    System.out.println("Serach Scenario is Started");
			logger.log(LogStatus.INFO, "Serach started");		
	driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.view.ViewGroup/android.support.v4.view.ViewPager/android.view.ViewGroup/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.TextView")).click();
	logger.log(LogStatus.INFO, "selected search field");
	driver.findElement(By.id("org.wikipedia:id/search_src_text")).click();
	logger.log(LogStatus.INFO, "Tapped on search field");
	driver.findElement(By.id("org.wikipedia:id/search_src_text")).sendKeys("Tesla model x");
	driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout[1]/android.view.ViewGroup/android.support.v4.view.ViewPager/android.view.ViewGroup/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.TextView")).click();
	logger.log(LogStatus.INFO, "Selected on 'Tesla model x' text");
	logger.log(LogStatus.INFO, "Result page varified");
	logger.log(LogStatus.PASS, "Search Test Completed successfully");
	driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click();
	Thread.sleep(5000);
	logger.log(LogStatus.PASS, "Redirected to home page");
	report.endTest(logger);
	System.out.println("Search scenario completed successfully");

		}catch (Exception e) {
			// TODO: handle exception
			logger.log(LogStatus.FAIL, "--------Test Failed, Here is the Exception Log--------\n"+e);
		}

	}

	public void login() throws Exception{
		try {
		    System.out.println("login Scenario is Started");
		logger = report.startTest("Login");
		logger.log(LogStatus.INFO, "login started");
		logger.log(LogStatus.PASS, "App launched successfully");	
		driver.findElement(By.id("org.wikipedia:id/fragment_onboarding_skip_button")).click();
		logger.log(LogStatus.INFO, "Tapped on skip button");
		driver.findElement(By.id("org.wikipedia:id/view_announcement_action_positive")).click();
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.FrameLayout/android.widget.EditText")).sendKeys("steenpjames5");
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.EditText")).sendKeys("Max@123$");
		driver.findElement(By.id("org.wikipedia:id/login_button")).click();
		report.endTest(logger);
		System.out.println("login Scenario is completed successfully");
		}catch (Exception e) {
			// TODO: handle exception
			logger.log(LogStatus.FAIL, "--------Test Failed, Here is the Exception Log--------/n"+e);
		}
		}


	public void logout() throws Exception{
		
		try {
		    System.out.println("Logout Scenario is Started");
		logger = report.startTest("Logout");
			
		
		driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Open\"]")).click();
		driver.findElement(By.id("org.wikipedia:id/main_drawer_login_button")).click();
		logger.log(LogStatus.PASS, "logged out Successfully");
		report.endTest(logger);
		System.out.println("Logout Scenario is complated successfully");
		}catch (Exception e) {
			// TODO: handle exception
			logger.log(LogStatus.FAIL, "--------Test Failed, Here is the Exception Log--------/n"+e);
		}
		}

	public void customize() throws Exception{
		
		try {
		    System.out.println("Field customization Scenario is Started");
		logger = report.startTest("Field Customization");
		logger.log(LogStatus.INFO, "Customization started");	
		
		driver.findElement(By.id("org.wikipedia:id/view_announcement_action_positive")).click();
		logger.log(LogStatus.INFO, "Tapped on customization button");
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.Switch")).click();
		logger.log(LogStatus.INFO, "Disabled 'In the News feed'");
		driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click();
		report.endTest(logger);
		System.out.println("Field customization Scenario is completed successfully");
		}catch (Exception e) {
			// TODO: handle exception
			logger.log(LogStatus.FAIL, "--------Test Failed, Here is the Exception Log--------/n"+e);
		}
		
		}

	public void myList() throws Exception{
		
		try {
		    System.out.println("My list view Scenario is Started");
		logger = report.startTest("View My List");
		
		
		
		
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"My lists\"]/android.widget.ImageView")).click();
		logger.log(LogStatus.INFO, "Tapped on My list icon");
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.TextView")).click();
		logger.log(LogStatus.PASS, "My list Verified");
		//android.widget.FrameLayout[@content-desc="Explore"]/android.widget.ImageView
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Explore\"]/android.widget.ImageView")).click();
		report.endTest(logger);
		System.out.println("My list view Scenario is complated successfully");
		}catch (Exception e) {
			// TODO: handle exception
			logger.log(LogStatus.FAIL, "--------Test Failed, Here is the Exception Log--------/n"+e);
		}
		
		}

	public void history() throws Exception{
		
		try {
		    System.out.println("History view Scenario is Started");
		logger = report.startTest("View History");
		
		
		
		
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"History\"]/android.widget.ImageView")).click();
		logger.log(LogStatus.INFO, "Tapped on History icon");
		logger.log(LogStatus.PASS, "History list Verified");
		//android.widget.FrameLayout[@content-desc="Explore"]/android.widget.ImageView
		driver.findElement(By.xpath("//android.widget.FrameLayout[@content-desc=\"Explore\"]/android.widget.ImageView")).click();
		report.endTest(logger);
		System.out.println("History view Scenario is completed successfully");
		}catch (Exception e) {
			// TODO: handle exception
			logger.log(LogStatus.FAIL, "--------Test Failed, Here is the Exception Log--------/n"+e);
		}
		
		}

	public void changeSettings() throws Exception{
		
		try {
		    System.out.println("Change setting Scenario is Started");
		logger = report.startTest("Settings Change");
		
		
		driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Open\"]")).click();
		
		driver.findElement(By.id("org.wikipedia:id/main_drawer_settings_container")).click();
		logger.log(LogStatus.INFO, "Tapped on Settings option");
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.Switch")).click();
		logger.log(LogStatus.INFO, "Disabled 'Show Link previews'");
		driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click();
		report.endTest(logger);
		}catch (Exception e) {
			// TODO: handle exception
			logger.log(LogStatus.FAIL, "--------Test Failed, Here is the Exception Log--------/n"+e);
		}
		
		}
	public void addLanguage() throws Exception{
		
		try {
		    System.out.println("Add language Scenario is Started");
		logger = report.startTest("Add Language");
		
		
		driver.findElement(By.id("org.wikipedia:id/add_lang_container")).click();
		
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[3]/android.widget.ImageView")).click();
		logger.log(LogStatus.INFO, "Tapped on add option");
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.TextView[1]")).click();
		logger.log(LogStatus.INFO, "Selected 'Espaniol''");
		driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]")).click();
		logger.log(LogStatus.PASS, "Language added successfully");
		report.endTest(logger);
		System.out.println("Add language Scenario is complted successfully");
		}catch (Exception e) {
			// TODO: handle exception
			logger.log(LogStatus.FAIL, "--------Test Failed, Here is the Exception Log--------/n"+e);
		}
		
		}



	public static void main(String[] args)throws Exception, MalformedURLException, InterruptedException{
	// TODO Auto-generated method stub

	wikiTest obj=new wikiTest();
	obj.setup();
	obj.starter();
	obj.addLanguage();
	obj.login();
	obj.myList();
	obj.history();
	obj.search();
	obj.customize();
	obj.changeSettings();
	obj.logout();
	System.out.println("-----------Test pack runnig completed----------------");

	report.flush();

	}	


	}
	
